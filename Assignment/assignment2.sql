CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
    RETURN NUMBER;
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS 
    FUNCTION price_after_discount(book_isbn IN VARCHAR2) 
    RETURN NUMBER IS 
        price_discounted NUMBER;
    BEGIN
        select (retail - discount)
            into price_discounted
            from Books
            where isbn = book_isbn;
        RETURN price_discounted;
    END price_after_discount;

    FUNCTION get_price_after_tax(book_isbn IN VARCHAR2) 
    RETURN NUMBER IS 
        price_taxed NUMBER;
        price_discounted NUMBER;
    BEGIN
        price_discounted := price_after_discount(book_isbn);
        price_taxed := price_discounted * 1.15;
        RETURN price_taxed;
    END get_price_after_tax;
END book_store;
/

DECLARE 
    first_book_isbn VARCHAR2(13); 
    second_book_isbn VARCHAR2(13); 
    price_after_tax_first_book NUMBER(5,2);
    price_after_tax_second_book NUMBER(5,2);
BEGIN
    SELECT isbn 
        INTO first_book_isbn 
        FROM Books 
    WHERE
        title = 'BUILDING A CAR WITH TOOTHPICKS';
    
    SELECT isbn 
        INTO second_book_isbn 
        FROM Books 
    WHERE
        title = 'HOLY GRAIL OF ORACLE';
    
    price_after_tax_first_book := book_store.get_price_after_tax(first_book_isbn);
    price_after_tax_second_book := book_store.get_price_after_tax(second_book_isbn);
    
    dbms_output.put_line('Price after tax for the first book: ' || price_after_tax_first_book);
    dbms_output.put_line('Price after tax for the second book: ' || price_after_tax_second_book);
END;
/
