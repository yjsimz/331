--DROP TABLE Area
DROP TABLE Person;
DROP TABLE Home;
DROP TABLE Occupation;

--CREATE TABLE Area
CREATE TABLE Home
(
    hid        CHAR(4)   PRIMARY KEY,      
    address    VARCHAR(200)
);

CREATE TABLE Occupation
(
    oid              CHAR(4)   PRIMARY KEY,        
    type             VARCHAR(20), 
    salary           NUMBER(10,2)
);

CREATE TABLE Person
(
    pid          CHAR(4)    PRIMARY KEY,  
    firstname    VARCHAR2(20),   
    lastname     VARCHAR2(20),
    father_id    CHAR(4)  REFERENCES Person (pid),
    mother_id    CHAR(4)  REFERENCES Person (pid),
    hid          CHAR(4)  REFERENCES Home (hid),
    oid          CHAR(4)  REFERENCES Occupation (oid)
);

-- Insert
INSERT INTO Home (hid, address) VALUES ('H001', '123 Easy St.');
INSERT INTO Home (hid, address) VALUES ('H002', '56 Fake Ln');

INSERT INTO Occupation (oid, type, salary) VALUES ('O000', 'N/A', 0);
INSERT INTO Occupation (oid, type, salary) VALUES ('O001', 'Student', 0);
INSERT INTO Occupation (oid, type, salary) VALUES ('O002', 'Doctor', 100000);
INSERT INTO Occupation (oid, type, salary) VALUES ('O003', 'Professor', 80000);

INSERT INTO Person (pid, firstname, lastname, hid, oid) VALUES ('P001', 'Zachary', 'Aberny', 'H002', 'O000');
INSERT INTO Person (pid, firstname, lastname, hid, oid) VALUES ('P002', 'Yanni', 'Aberny', 'H002', 'O000');
INSERT INTO Person (pid, firstname, lastname, father_id, mother_id, hid, oid) VALUES ('P003', 'Alice', 'Aberny', 'P001', 'P002', 'H001', 'O002');
INSERT INTO Person (pid, firstname, lastname, hid, oid) VALUES ('P004', 'Bob', 'Bortelson', 'H001', 'O003');
INSERT INTO Person (pid, firstname, lastname, father_id, mother_id, hid, oid) VALUES ('P005', 'Carl', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');
INSERT INTO Person (pid, firstname, lastname, father_id, mother_id, hid, oid) VALUES ('P006', 'Denise', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');

SELECT gp.firstname FROM Person gp JOIN Person p ON gp.pid = p.mother_id OR gp.pid = p.father_id JOIN Person c ON p.pid = c.mother_id OR p.pid = c.father_id WHERE c.firstname = 'Denise';

--Q1
SELECT h.address, COUNT(p.pid) AS Number_Of_People_Each_Household
FROM Home h 
INNER JOIN Person p 
ON p.hid=h.hid 
GROUP BY h.address;

--Q2
SELECT (p.firstname || ' ' || p.lastname) AS fullname 
FROM Person p
JOIN Person pp
ON p.pid=pp.father_id;

--Q3
SELECT (p.firstname || ' ' || p.lastname) AS full_name , COUNT(pp.pid) AS Number_of_Parents
FROM Person p 
JOIN Person pp
ON pp.pid = p.father_id OR pp.pid=p.mother_id
WHERE p.oid='O001' AND p.hid = pp.hid
GROUP BY p.pid, p.firstname, p.lastname
HAVING COUNT(pp.pid)>1;

--Q4
SELECT SUM(o.salary) AS Household_Income 
FROM Occupation o 
INNER JOIN Person p 
ON o.oid=p.oid 
GROUP BY p.hid 
ORDER BY Household_Income DESC
FETCH FIRST ROW ONLY; 



